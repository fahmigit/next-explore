import axios from "axios";
import Link from "next/link";
import { React, useState, useEffect } from "react";
import {useForm} from 'react-hook-form'

function create() {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors:any },
  } = useForm();

  const [result, setresult] = useState({});
  const [error, setError] = useState('')
  const [buttonText, setbuttonText] = useState("Submit");
  const baseURL = "https://jsonplaceholder.typicode.com/posts";
  const input = {
    padding: "3px 5px",
    borderRadius: "5px",
    border: "solid",
  };
  const submitForm = async (data) => {
    console.log(data);
    //e.preventDefault();
    setbuttonText("Loading...");

    // CONTOH FETCH TANPA VARIABEL
    //   await axios.post(baseURL, data).then((res) => {
    //     setresult(res.data);
    //     setbuttonText("Submit");
    //   }).catch(error=>{
    //     setbuttonText("Submit");
    //     console.log(error);
    //     setError(error.message);
    // });

    // CONTOH FETCH PAKAI VARIABEL
    const response = await axios.post(baseURL, data).catch(error=>{
        setbuttonText("Submit");
        console.log(error);
        setError(error.message);
    });
    if (response) {
      setresult(response.data);
      setbuttonText("Submit");
    } else {
      setbuttonText("Submit");
    }
  };
  return (
    <div className="m-5">
      <Link href="/jsontes">
        <a className="px-4 py-2 bg-gray-300 rounded-md">Back</a>
      </Link>
      <h1 className="text-lg my-3">Create post</h1>
      <br />
      <p className="text-red-400">{error ? error : ""}</p>
      <form type="post" onSubmit={handleSubmit(submitForm)}>
        <input
          style={input}
          type="text"
          placeholder="input title"
          {...register("title", {
            required: true,
            pattern: /^[A-Za-z]+$/i,
          })}
        />
        <p className="text-red-400">
          {errors.title && (
            <span>This field title is required & wajib huruf</span>
          )}
        </p>
        <input
          style={input}
          type="text"
          placeholder="input body"
          {...register("body", { required: true })}
        />
        <p className="text-red-400">
          {errors.body && <span>This field body is required</span>}
        </p>
        <button className="px-2 bg-blue-400 rounded-md text-white">
          {buttonText}
        </button>
        <hr />
        <div>
          Result
          <br />
          result ID : {result.id} <br />
          result Title : {result.title} <br />
          result Body : {result.body}
        </div>
      </form>
    </div>
  );
}

export default create;
