import {useState, useEffect} from 'react'
import axios from 'axios'
import Link from 'next/link'

function Index() {
    const [data, setData] = useState([]);
    const baseURL = "https://jsonplaceholder.typicode.com/posts/";
    useEffect(() => {
      const getData = async () => {
        await axios.get(baseURL).then((res) => {
            console.log(res);
            setData(res.data)
        })
      }
      getData();
    }, [])
    
    // Style CSS
    const box = {
      border: "solid",
      padding: "1rem",
      marginBottom: "1rem"
    };
    const hsatu = {
      fontSize: "30px",
      fontWeight: "bold",
    };
    const container = {
      margin: "2rem",
    };
  return (
    <div style={container}>
      <Link href="/">
        <a className="px-4 py-2 bg-gray-300 rounded-md">Back</a>
      </Link>
      <h1 style={hsatu}>List Data Post from JsonPlaceholder</h1>
      <Link href="/jsontes/create">
        <a className="px-4 py-2 bg-green-400 rounded-md text-white">
          Create Post
        </a>
      </Link>
      {data.map((value, index) => {
        return (
          <div className="mt-2" style={box} key={index}>
            <h1>{value.title}</h1>
            <h3>{value.id}</h3>
            <p>{value.body}</p>
          </div>
        );
      })}
    </div>
  );
}

export default Index