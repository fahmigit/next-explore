import {React, useEffect} from 'react'
import Link from 'next/link'

function index() {

    useEffect(() => {
      const inputs = document.querySelectorAll("input");
      const output = document.getElementById("output");
      console.log(inputs);
      inputs.forEach((input, key) => {
        input.addEventListener("keyup", function () {
          if (input.value) {
            console.log(key);
            if (key === 5) {
              const userCode = [...inputs].map((input) => input.value).join("");
              output.classList.remove("hidden");
              output.innerText = userCode;
            } else {
              inputs[key + 1].focus();
            }
          }
        });
      });

    }, []);

    const handleDelete = (event) => {
      const inputs = document.querySelectorAll("input");
      const output = document.getElementById("output");
      inputs.forEach((input, key) => {
        input.addEventListener("keyup", function () {
          if (input.value) {
            console.log(event);
            if (key === 5) {
              if (event.key === "Backspace") {
                console.log("Backspace key pressed ✅");
              }
            } else {
              inputs[key - 1].focus();
            }
          }
        });
      });
    };

    return (
      <>
        <div className="relative h-screen flex justify-center items-center">
          <div className="space-y-4">
            <div className="absolute top-4 text-center">
              <Link className="flex-none" href="/">
                <a className="px-5 py-2 border-2 rounded-md hover:bg-slate-100">
                  Back
                </a>
              </Link>
            </div>
            <h1 className="text-center">Input your PIN</h1>
            <form className="flex">
              <input
                className="p-2 border-2 rounded-lg w-12 text-center mx-2"
                type="password"
                maxLength="1"
                onKeyDown={handleDelete}
              />
              <input
                className="p-2 border-2 rounded-lg w-12 text-center mx-2"
                type="password"
                maxLength="1"
              />
              <input
                className="p-2 border-2 rounded-lg w-12 text-center mx-2"
                type="password"
                maxLength="1"
              />
              <input
                className="p-2 border-2 rounded-lg w-12 text-center mx-2"
                type="password"
                maxLength="1"
              />
              <input
                className="p-2 border-2 rounded-lg w-12 text-center mx-2"
                type="password"
                maxLength="1"
              />
              <input
                className="p-2 border-2 rounded-lg w-12 text-center mx-2"
                type="password"
                maxLength="1"
              />
            </form>
            <div className="my-3 hidden" id="output"></div>
          </div>
        </div>
      </>
    );
}

export default index